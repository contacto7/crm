<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    const CALL = 1;
    const EMAIL = 2;
    const VISIT = 3;

    protected $fillable = [
        'name', 'icon'
    ];

    public function activities(){
        return $this->hasMany(Activity::class);
    }

}
