<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    const SGAS = 1;
    const SGHS = 2;
    const SGCN = 3;
    const SGCA = 4;
    const SGC = 5;
    const SST = 6;
    const RSO = 7;
    const AMB = 8;

    protected $fillable = [
        'name', 'description', 'price',
    ];
    /**
     * Get the serviceType's price.
     *
     * @param  string  $value
     * @return string
     */
    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ".","'");
    }

    public function opportunities(){
        return $this
            ->belongsToMany(Opportunity::class)
            ->withTimestamps();
    }
}
