<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':
                return [
                    'company_name' => 'required|min:2',
                    'tax_number' => 'required|numeric',
                    'address' => 'nullable',
                    'turn' => 'nullable',
                    'phone' => 'nullable',
                    'manager_name' => 'nullable',
                    'manager_last_name' => 'nullable',
                    'manager_dni' => 'nullable',
                ];
        }
    }
}
