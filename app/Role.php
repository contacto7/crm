<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name','description',
    ];

    const SUPER_ADMIN = 1;
    const ADMIN = 2;
    const SUPERVISOR = 3;
    const COMMERCIAL = 4;

    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }
}
