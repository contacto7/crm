<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        $isAdmin = $user->role_id === Role::SUPER_ADMIN || $user->role_id === Role::ADMIN;
        return $isAdmin;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user)
    {
        $isAdmin = $user->role_id === Role::SUPER_ADMIN || $user->role_id === Role::ADMIN;
        return $isAdmin;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $isAdmin = $user->role_id === Role::SUPER_ADMIN || $user->role_id === Role::ADMIN;
        return $isAdmin;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        //IF ADMIN, AND UPDATED IS NOT SUPER_ADMIN
        $isAdmin = $user->role_id === Role::ADMIN;
        $userUpdatedIsNOTSuperAdmin = $model->role_id != Role::SUPER_ADMIN;
        //IF SUPER_ADMIN
        $isSuperAdmin = $user->role_id === Role::SUPER_ADMIN;
        return ($isAdmin && $userUpdatedIsNOTSuperAdmin) || $isSuperAdmin;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }
}
