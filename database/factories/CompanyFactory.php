<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'tax_number' => $faker->numerify('20#########'),
        'address' => $faker->address,
        'turn' => $faker->catchPhrase,
        'phone' => $faker->phoneNumber,
        'manager_name' => $faker->name,
        'manager_last_name' => $faker->lastName,
        'manager_dni' => $faker->numerify('########'),
        'user_id'=>\App\User::all()->random()->id,
    ];
});
