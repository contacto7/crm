<?php

use App\CompanyContact;
use App\Survey;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::SUPER_ADMIN,
            'name' => 'super-admin',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::ADMIN,
            'name' => 'admin',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::SUPERVISOR,
            'name' => 'supervisor',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::COMMERCIAL,
            'name' => 'commercial',
        ]);

        //USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Super Admin',
            'email' => 'super_admin@mail.com',
            'role_id' => \App\Role::SUPER_ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'role_id' => \App\Role::ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Supervisor',
            'email' => 'supervisor@mail.com',
            'role_id' => \App\Role::SUPERVISOR,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Commercial',
            'email' => 'commercial@mail.com',
            'role_id' => \App\Role::COMMERCIAL,
            'state' => \App\User::ACTIVE,
        ]);


        //RANDOM USERS
        factory(\App\User::class, 3)->create();

        //RANDOM COMPANIES AND COMPANY CONTACTS
        factory(\App\Company::class, 20)
            ->create()
            ->each(function (\App\Company $co) {
                factory(\App\CompanyContact::class, 1)
                    ->create([
                        'company_id'=>$co->id,
                        'principal'=>CompanyContact::PRINCIPAL,
                    ]);
                factory(\App\CompanyContact::class, 2)
                    ->create([
                        'company_id'=>$co->id,
                        'principal'=>CompanyContact::SECONDARY,
                    ]);
            });


        //SURVEYS
        ///////////ACTIVE
        factory(\App\Survey::class, 1)
            ->create([
                'name'=>"Encuesta de satisfacción AB01-AS23",
                'state'=>Survey::ACTIVE,
            ])
            ->each(function (\App\Survey $su) {
                //QUESTIONS
                factory(\App\Question::class, 5)
                    ->create([
                        'survey_id'=>$su->id,
                    ])
                    //OPTIONS
                    ->each(function (\App\Question $qu) {
                        for ($i = 1; $i <= 5; $i++) {
                            factory(\App\Option::class, 5)
                                ->create([
                                    'type' => $i,
                                    'question_id' => $qu->id,
                                ]);
                        }
                    });
            });

        //RANDOM ANSWERS
        factory(\App\Answer::class, 100)->create();


        ///////////INACTIVE
        factory(\App\Survey::class, 5)
            ->create([
                'state'=>Survey::INACTIVE,
            ])
            ->each(function (\App\Survey $su) {
                //QUESTIONS
                factory(\App\Question::class, 5)
                    ->create([
                        'survey_id'=>$su->id,
                    ])
                    //OPTIONS
                    ->each(function (\App\Question $qu) {
                        for ($i = 1; $i <= 5; $i++) {
                            factory(\App\Option::class, 5)
                                ->create([
                                    'type' => $i,
                                    'question_id' => $qu->id,
                                ]);
                        }
                    });
            });
        //RANDOM ANSWERS
        factory(\App\Answer::class, 200)->create();


        //CAMPAIGN TYPES
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::OTHER,
            'name' => 'Otro',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SOCIAL_MEDIA,
            'name' => 'Red Social',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::MAILING,
            'name' => 'Correos',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SEM,
            'name' => 'Google Ads',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SEO,
            'name' => 'Búsqueda orgánica',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::REFERRED,
            'name' => 'Referido',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::CALLING,
            'name' => 'LLamada',
        ]);

        //CAMPAIGN RANDOM
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::OTHER,
            'name' => 'Otro',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SOCIAL_MEDIA,
            'name' => 'Publicidad den Facebook',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::MAILING,
            'name' => 'Mailling masivo a empresas limeñas',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SEM,
            'name' => 'Publicidad en Google',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SEO,
            'name' => 'SEO realizado en Web',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::REFERRED,
            'name' => 'Referido de cliente',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::CALLING,
            'name' => 'Llamadas a clientes potenciales de Lima',
        ]);

        //SERVICE TYPES
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGAS,
            'name' => 'SGAS',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGHS,
            'name' => 'SGHS',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGCN,
            'name' => 'SGCN',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGCA,
            'name' => 'SGCA',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGC,
            'name' => 'SGC',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SST,
            'name' => 'Seguridad y Salud en el Trabajo',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::RSO,
            'name' => 'Responsabilidad Social',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::AMB,
            'name' => 'Ambiental',
        ]);

        //STAGE
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::FIRST_MEETING,
            'name' => 'Primera reunión',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::SEND_PROPOSE,
            'name' => 'Preparando propuesta',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::PROPOSE_SENT,
            'name' => 'Propuesta enviada',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::PRICES_NEGOTIATION,
            'name' => 'Negociación de precios',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::CLOSED_WON,
            'name' => 'Negocio ganado',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::CLOSED_LOST,
            'name' => 'Negocio perdido',
        ]);

        //OPPORTUNITY TYPES
        factory(\App\OpportunityType::class, 1)->create([
            'id' => \App\OpportunityType::NEW_CUSTOMER,
            'name' => 'Cliente nuevo',
        ]);
        factory(\App\OpportunityType::class, 1)->create([
            'id' => \App\OpportunityType::EXISTING_CUSTOMER,
            'name' => 'Cliente existente',
        ]);

        //RANDOM OPPORTUNITIES
        factory(\App\Opportunity::class, 250)->create();


        //////////ASSIGNING SERVICES TYPES TO OPPORTUNITIES
        //$serviceTypesArr = \App\ServiceType::pluck('id')->toArray();
        foreach(\App\Opportunity::get() as $opportunity){
            $randomServiceNumber = rand(\App\ServiceType::SGAS, \App\ServiceType::AMB);
            $serviceTypesArr = \App\ServiceType::inRandomOrder()->limit($randomServiceNumber)->pluck('id')->toArray();
            $opportunity->serviceTypes()->sync($serviceTypesArr);
        }


        //ACTIVITY TYPES
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::CALL,
            'name' => 'Llamada',
            'icon' => 'phone',
        ]);
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::EMAIL,
            'name' => 'Correo',
            'icon' => 'envelope',
        ]);
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::VISIT,
            'name' => 'Visita',
            'icon' => 'group',
        ]);

        //RANDOM ACTIVITIES
        factory(\App\Activity::class, 50)->create();




    }
}
