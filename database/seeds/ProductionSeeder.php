<?php

use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::SUPER_ADMIN,
            'name' => 'super-admin',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::ADMIN,
            'name' => 'admin',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::SUPERVISOR,
            'name' => 'supervisor',
        ]);
        factory(\App\Role::class, 1)->create([
            'id' => \App\Role::COMMERCIAL,
            'name' => 'commercial',
        ]);

        //USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Contacto',
            'last_name' => 'Inoloop',
            'email' => 'contacto@inoloop.com',
            'cellphone' => '+51 961518524',
            'password' => 'secret',
            'role_id' => \App\Role::SUPER_ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Billy',
            'last_name' => 'Areche',
            'email' => 'billy.areche@thalescorp.pe',
            'cellphone' => '+51 981242195',
            'password' => 'secret',
            'role_id' => \App\Role::SUPER_ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Jerson',
            'last_name' => 'Landeo',
            'email' => 'comercial@thalescorp.pe',
            'cellphone' => '+51 922265209',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => \App\User::ACTIVE,
        ]);


        //CAMPAIGN TYPES
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::OTHER,
            'name' => 'Otro',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SOCIAL_MEDIA,
            'name' => 'Red Social',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::MAILING,
            'name' => 'Correos',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SEM,
            'name' => 'Google Ads',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::SEO,
            'name' => 'Búsqueda orgánica',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::REFERRED,
            'name' => 'Referido',
        ]);
        factory(\App\CampaignType::class, 1)->create([
            'id' => \App\CampaignType::CALLING,
            'name' => 'LLamada',
        ]);

        //CAMPAIGN RANDOM
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::OTHER,
            'name' => 'Otro',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SOCIAL_MEDIA,
            'name' => 'Publicidad den Facebook',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::MAILING,
            'name' => 'Mailling masivo a empresas limeñas',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SEM,
            'name' => 'Publicidad en Google',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::SEO,
            'name' => 'SEO realizado en Web',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::REFERRED,
            'name' => 'Referido de cliente',
        ]);
        factory(\App\Campaign::class, 1)->create([
            'campaign_type_id' => \App\CampaignType::CALLING,
            'name' => 'Llamadas a clientes potenciales de Lima',
        ]);


        //SERVICE TYPES
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGAS,
            'name' => 'SGAS',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGHS,
            'name' => 'SGHS',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGCN,
            'name' => 'SGCN',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGCA,
            'name' => 'SGCA',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SGC,
            'name' => 'SGC',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::SST,
            'name' => 'Seguridad y Salud en el Trabajo',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::RSO,
            'name' => 'Responsabilidad Social',
        ]);
        factory(\App\ServiceType::class, 1)->create([
            'id' => \App\ServiceType::AMB,
            'name' => 'Ambiental',
        ]);


        //STAGE
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::FIRST_MEETING,
            'name' => 'Primera reunión',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::SEND_PROPOSE,
            'name' => 'Preparando propuesta',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::PROPOSE_SENT,
            'name' => 'Propuesta enviada',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::PRICES_NEGOTIATION,
            'name' => 'Negociación de precios',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::CLOSED_WON,
            'name' => 'Negocio ganado',
        ]);
        factory(\App\Stage::class, 1)->create([
            'id' => \App\Stage::CLOSED_LOST,
            'name' => 'Negocio perdido',
        ]);

        //SERVICE TYPES
        factory(\App\OpportunityType::class, 1)->create([
            'id' => \App\OpportunityType::NEW_CUSTOMER,
            'name' => 'Cliente nuevo',
        ]);
        factory(\App\OpportunityType::class, 1)->create([
            'id' => \App\OpportunityType::EXISTING_CUSTOMER,
            'name' => 'Cliente existente',
        ]);


        //ACTIVITY TYPES
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::CALL,
            'name' => 'Llamada',
            'icon' => 'phone',
        ]);
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::EMAIL,
            'name' => 'Correo',
            'icon' => 'envelope',
        ]);
        factory(\App\ActivityType::class, 1)->create([
            'id' => \App\ActivityType::VISIT,
            'name' => 'Visita',
            'icon' => 'group',
        ]);



    }
}
