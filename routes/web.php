<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function (){

    Route::group(['prefix' => 'users'], function (){
        Route::get('/listAll','UserController@listAll')
            ->name('users.listAll');
        Route::get('/admin/{id}','UserController@admin')
            ->name('users.admin');
        Route::get('/edit/{id}','UserController@edit')
            ->name('users.edit');
        Route::get('/search','UserController@filter')
            ->name('users.search');
        Route::get('/help','UserController@help')
            ->name('users.help');
        Route::get('/create','UserController@create')
            ->name('users.create');
        Route::post('/store','UserController@store')
            ->name('users.store');
        Route::get('/edit/{id}','UserController@edit')
            ->name('users.edit');
        Route::put('/update/{user}','UserController@update')
            ->name('users.update');
        Route::put('/help','UserController@help')
            ->name('users.help');

    });
    Route::group(['prefix' => 'opportunities'], function (){
        Route::get('/dashboard/{userDashboard?}','OpportunityController@dashboard')
            ->name('opportunities.dashboard');
        Route::get('/listAll','OpportunityController@listAll')
            ->name('opportunities.listAll');
        Route::get('/searchList','OpportunityController@filterList')
            ->name('opportunities.searchList');
        Route::get('/listCompany/{company}','OpportunityController@listCompany')
            ->name('opportunities.listCompany');
        Route::get('/listStageAjax','OpportunityController@listStageAjax')
            ->name('opportunities.listStageAjax');
        Route::get('/getStageTotalAjax','OpportunityController@getStageTotalAjax')
            ->name('opportunities.getStageTotalAjax');
        Route::get('/updateOpportunityAjax','OpportunityController@updateOpportunityAjax')
            ->name('opportunities.updateOpportunityAjax');
        Route::get('/viewOpportunity/{id}','OpportunityController@viewOpportunity')
            ->name('opportunities.viewOpportunity');
        Route::post('/store','OpportunityController@store')
            ->name('opportunities.store');
        Route::put('/update/{opportunity}','OpportunityController@update')
            ->name('opportunities.update');
        Route::get('/search/{userDashboard?}','OpportunityController@filter')
            ->name('opportunities.search');
    });
    Route::group(['prefix' => 'activities'], function (){
        Route::post('/store','ActivityController@store')
            ->name('activities.store');
    });
    Route::group(['prefix' => 'companies'], function (){
        Route::get('/search','CompanyController@filter')
            ->name('companies.search');
        Route::get('/listAll','CompanyController@listAll')
            ->name('companies.listAll');
        Route::get('/admin/{id}','CompanyController@admin')
            ->name('companies.admin');
        Route::get('/user/{id}','CompanyController@user')
            ->name('companies.user');
        Route::get('/create','CompanyController@create')
            ->name('companies.create');
        Route::post('/store','CompanyController@store')
            ->name('companies.store');
        Route::get('/edit/{id}','CompanyController@edit')
            ->name('companies.edit');
        Route::put('/update/{company}','CompanyController@update')
            ->name('companies.update');
    });
    Route::group(['prefix' => 'company-contacts'], function (){
        Route::get('/search','CompanyContactController@filter')
            ->name('companyContacts.search');
        Route::get('/listAll','CompanyContactController@listAll')
            ->name('companyContacts.listAll');
        Route::get('/listCompany/{company}','CompanyContactController@listCompany')
            ->name('companyContacts.listCompany');
        Route::get('/admin/{id}','CompanyContactController@admin')
            ->name('companyContacts.admin');
        Route::get('/user/{id}','CompanyContactController@user')
            ->name('companyContacts.user');
        Route::get('/create/{company?}','CompanyContactController@create')
            ->name('companyContacts.create');
        Route::post('/store','CompanyContactController@store')
            ->name('companyContacts.store');
        Route::get('/edit/{id}','CompanyContactController@edit')
            ->name('companyContacts.edit');
        Route::put('/update/{companyContact}','CompanyContactController@update')
            ->name('companyContacts.update');
        Route::get('/getCompanyContactsAjax','CompanyContactController@getCompanyContactsAjax')
            ->name('companyContacts.getCompanyContactsAjax');
    });
    Route::group(['prefix' => 'campaigns'], function (){
        Route::get('/listAll','CampaignController@listAll')
            ->name('campaigns.listAll');
        Route::get('/search','CampaignController@filter')
            ->name('campaigns.search');
        Route::get('/admin/{id}','CampaignController@admin')
            ->name('campaigns.admin');
        Route::get('/create','CampaignController@create')
            ->name('campaigns.create');
        Route::post('/store','CampaignController@store')
            ->name('campaigns.store');
        Route::get('/edit/{id}','CampaignController@edit')
            ->name('campaigns.edit');
        Route::put('/update/{campaign}','CampaignController@update')
            ->name('campaigns.update');
    });
    Route::group(['prefix' => 'surveys'], function (){
        Route::get('/listAll','SurveyController@listAll')
            ->name('surveys.listAll');
        Route::get('/admin/{id}','SurveyController@admin')
            ->name('surveys.admin');
        Route::get('/search','SurveyController@filter')
            ->name('surveys.search');
        Route::get('/create','SurveyController@create')
            ->name('surveys.create');
        Route::post('/store','SurveyController@store')
            ->name('surveys.store');
        Route::get('/addQuestions/{id}','SurveyController@addQuestions')
            ->name('surveys.addQuestions');
        Route::get('/results/{id}','SurveyController@results')
            ->name('surveys.results');
        Route::get('/edit/{id}','SurveyController@edit')
            ->name('surveys.edit');
        Route::put('/update/{id}','SurveyController@update')
            ->name('surveys.update');
    });
    Route::group(['prefix' => 'questions'], function (){
        Route::get('/create','QuestionController@create')
            ->name('questions.create');
        Route::post('/store','QuestionController@store')
            ->name('questions.store');
        Route::get('/edit/{id}','QuestionController@edit')
            ->name('questions.edit');
        Route::put('/update/{campaign}','QuestionController@update')
            ->name('questions.update');
    });
    Route::group(['prefix' => 'service-types'], function (){
        Route::get('/listAll','ServiceTypeController@listAll')
            ->name('serviceTypes.listAll');
        Route::get('/search','ServiceTypeController@filter')
            ->name('serviceTypes.search');
        Route::get('/admin/{id}','ServiceTypeController@admin')
            ->name('serviceTypes.admin');
        Route::get('/create','ServiceTypeController@create')
            ->name('serviceTypes.create');
        Route::post('/store','ServiceTypeController@store')
            ->name('serviceTypes.store');
        Route::get('/edit/{id}','ServiceTypeController@edit')
            ->name('serviceTypes.edit');
        Route::put('/update/{serviceType}','ServiceTypeController@update')
            ->name('serviceTypes.update');
    });


    //TEST ROUTES//
    Route::group(['prefix' => 'documents'], function (){
        Route::get('/quickstart','DocumentController@quickstart')
            ->name('documents.quickstart');
        Route::get('/testQuotation','DocumentController@testQuotation')
            ->name('documents.testQuotation');
        Route::get('/createOpportunityDocument/{opportunity}/{type}/{documentType}','DocumentController@createOpportunityDocument')
            ->name('documents.createOpportunityDocument');
        Route::get('/editDocument/{opportunity}/{type}/{documentType}','DocumentController@editDocument')
            ->name('documents.editDocument');
        Route::get('/downloadDocument/{opportunity}/{type}','DocumentController@downloadDocument')
            ->name('documents.downloadDocument');
    });


    //TEST ROUTES//
    Route::group(['prefix' => 'reports'], function (){
        Route::get('/dashboard/{year?}/{months?}','ReportController@dashboard')
            ->name('reports.dashboard');
        //WON DATA
        Route::get('/totalWon/{year?}/{months?}','ReportController@totalWon')
            ->name('reports.usersWon');
        Route::get('/usersWon/{year?}/{months?}','ReportController@usersWon')
            ->name('reports.usersWon');
        Route::get('/servicesWon/{year?}/{months?}','ReportController@servicesWon')
            ->name('reports.servicesWon');
        Route::get('/servicesWonPrice/{year?}/{months?}','ReportController@servicesWonPrice')
            ->name('reports.servicesWonPrice');
        Route::get('/companiesWon/{year?}/{months?}','ReportController@companiesWon')
            ->name('reports.companiesWon');
        //OPEN
        Route::get('/usersOpen','ReportController@usersOpen')
            ->name('reports.usersOpen');
        Route::get('/companiesOpen','ReportController@companiesOpen')
            ->name('reports.companiesOpen');
        //BY STAGE
        Route::get('/stagesTotal/{year?}/{months?}','ReportController@stagesTotal')
            ->name('reports.stagesTotal');

        Route::get('/usersOpportunitiesSum/{year?}/{months?}','ReportController@usersOpportunitiesSum')
            ->name('reports.usersOpportunitiesSum');
    });

});

Route::group(['prefix' => 'surveys'], function (){
    Route::get('/vote/{uid}','SurveyController@vote')
        ->name('surveys.vote');
    Route::post('/storeVote','SurveyController@storeVote')
        ->name('surveys.storeVote');
});
