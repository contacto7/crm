@extends('layouts.app')

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => __("Empresa"),
                    'icon' => "building"
                ])
            </div>
        </div>
        <form
            method="POST"
            action="{{ ! $company->id ? route('companies.store'): route('companies.update', $company->id) }}"
            novalidate
            enctype="multipart/form-data"
        >
            @if($company->id)
                @method('PUT')
            @endif

            @csrf



            <div class="form-group">
                <label for="company_name">Razón Social</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('company_name') ? 'is-invalid': '' }}"
                    name="company_name"
                    id="company_name"
                    value="{{ old('company_name') ?: $company->company_name }}"
                >
                @if($errors->has('company_name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('company_name') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="tax_number">RUC</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('tax_number') ? 'is-invalid': '' }}"
                    name="tax_number"
                    id="tax_number"
                    value="{{ old('tax_number') ?: $company->tax_number }}"
                >
                @if($errors->has('tax_number'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('tax_number') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="turn">Giro de la empresa</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('turn') ? 'is-invalid': '' }}"
                    name="turn"
                    id="turn"
                    value="{{ old('turn') ?: $company->turn }}"
                >
                @if($errors->has('turn'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('turn') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address">Dirección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    value="{{ old('address') ?: $company->address }}"
                >
                @if($errors->has('address'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="phone">Teléfono</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('phone') ? 'is-invalid': '' }}"
                    name="phone"
                    id="phone"
                    value="{{ old('phone') ?: $company->phone }}"
                >
                @if($errors->has('phone'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_name">Nombre de Gerente</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_name') ? 'is-invalid': '' }}"
                    name="manager_name"
                    id="manager_name"
                    value="{{ old('manager_name') ?: $company->manager_name }}"
                >
                @if($errors->has('manager_name'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('manager_name') }}</strong>
            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_last_name">Apellidos de Gerente</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_last_name') ? 'is-invalid': '' }}"
                    name="manager_last_name"
                    id="manager_last_name"
                    value="{{ old('manager_last_name') ?: $company->manager_last_name }}"
                >
                @if($errors->has('manager_last_name'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('manager_last_name') }}</strong>
            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="manager_dni">DNI de Gerente</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('manager_dni') ? 'is-invalid': '' }}"
                    name="manager_dni"
                    id="manager_dni"
                    value="{{ old('manager_dni') ?: $company->manager_dni }}"
                >
                @if($errors->has('manager_dni'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('manager_dni') }}</strong>
            </span>
                @endif
            </div>



            <div class="form-group">
                <button type="submit" class="btn btn-thales-secondary">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
@endsection

@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        /*
        * Función para agregar el nombre del archivo
        * al input file
        */
        $(document).on('change', '.custom-file-input', function(){
            console.log("Cambió");
            var filename = $(this).val().split('\\').pop();
            console.log(filename);
            $(this).siblings('.custom-file-label').html("<i>"+filename+"</i>");
        });


    </script>
@endpush
