@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('partials.campaigns.search')
        </div>
        <div class="row justify-content-center">
            <table class="table table-hover table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Campaña</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Creó</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($campaigns as $campaign)
                    <tr>
                        <td>{{ $campaign->id }}</td>
                        <td>{{ $campaign->name }}</td>
                        <td>{{ $campaign->campaignType->name }}</td>
                        <td>
                            @if($campaign->state == \App\Campaign::ACTIVE)
                                Activa
                            @elseif($campaign->state == \App\Campaign::INACTIVE)
                                Inactiva
                            @endif

                        </td>
                        <td>{!! $campaign->user->name."<br> ".$campaign->user->last_name !!}</td>
                        <td>
                            <div class="btn-group mb-2">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('campaigns.admin', $campaign->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Ver información de campaña"
                                >
                                    <i class="fa fa-info-circle"></i>
                                </a>
                                @can('update', [\App\Campaign::class, $campaign])
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('campaigns.edit', $campaign->id) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Editar información de campaña"
                                >
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay campañas disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <!-- Modal para las notas -->
        <!-- The Modal -->
        <div class="modal" id="modalNotes">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Notas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body modal-ajax-content"></div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            {{ $campaigns->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
@push('scripts')
@endpush
