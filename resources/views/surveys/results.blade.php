@extends('layouts.app', ['page' => 'surveys'])

@section('content')
    <div class="container container-form">
        <div class="row">
            <div class="head-page">
                @include('partials.title', [
                    'title' => "Resultados de ". $survey->name,
                    'icon' => "file-text-o"
                ])
            </div>
        </div>
        <div class="questions-wrap">
            <div class="questions-inn">
                <table class="table  table-hover table-bordered">
                    <thead>
                    <tr>
                        <th class="col-order text-center">Preguntas</th>
                        @for($i = 1 ; $i<= \App\Survey::QUESTIONS; $i++)
                            <th class="text-center">{{ $i }}</th>
                        @endfor
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($survey->questions as $question)
                        <tr class="candidate">
                            <td class="list- text-center">
                                {{ $question->name }}
                            </td>
                            @for($i = 1 ; $i<= \App\Survey::QUESTIONS; $i++)
                                <td class="text-center">
                                    <label class="" for="candidate_0_1">
                                        @if($question->options->where('type', $i)->first())
                                            {{ $question->options->where('type', $i)->first()->answers->count() }}
                                        @else
                                            0
                                        @endif
                                    </label>
                                </td>
                            @endfor
                        </tr>
                    @endforeach


                    </tbody>
                </table>

                <div class="form-group text-center">
                    <a type="submit" class="btn btn-danger" href="{{ route("surveys.listAll") }}">
                        {{ __("Siguiente") }}
                    </a>
                </div>


            </div>

        </div>


    </div>


@endsection

@push('scripts')
    <script>


    </script>
@endpush
