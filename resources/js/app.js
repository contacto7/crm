/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});



//OPPORTUNITY DASHBOARD
//DASHBOARD OPPORTUNITIES
window.addStageList =  function(stage_id, stage_list_route, opportunities_stage_html, opportunities_html_wrapper, search_input, userDashboard){
    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: stage_list_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            stage_id: stage_id,
            search_input: search_input,
            user_dashboard: userDashboard,
        },
        success: function (data) {
            //console.log("-----------------STAGE "+stage_id+"----------------");
            //console.log(data);
            $(data).each(function (index,value) {
                //console.log("--------STAGE OPP "+stage_id+"--------");
                //CLONE THE TEMPLATE
                let cloned_template = $("#opportunity-template .opportunity-wrapp").clone();
                //FILL TEMPLATE WITH DATA
                cloned_template.attr("data-id",value.id);
                cloned_template.attr("data-order",value.order);
                cloned_template.attr("data-price",value.service_price);
                cloned_template.find(".opportunity-single-title").html(value.name);
                cloned_template.find(".opportunity-single-company").html(value.company.company_name);
                cloned_template.find(".opportunity-single-price").html(value.service_price);
                //HREF TO VIEW OPPORTUNITY
                let view_opportunity_route = cloned_template.find(".view-opportunity").attr("data-href");
                view_opportunity_route = view_opportunity_route.replace('replace', value.id);
                cloned_template.find(".view-opportunity").attr("href", view_opportunity_route);
                //ADD ELEMENT TO STAGE LIST
                $("."+opportunities_html_wrapper+"-"+stage_id).append(cloned_template);
                //ADD SORTABLE FUNCTIONALITY TO ELEMENT
                makeSortable(opportunities_stage_html, opportunities_html_wrapper, stage_id);
                //console.log(value);
            });
            //console.log("-----------------STAGE "+stage_id+"----------------");
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });
};


window.makeSortable = function(opportunities_stage_html,opportunities_html_wrapper, stage_id) {

    $( "."+opportunities_stage_html ).each(function( index ) {
        $(this).find("."+opportunities_html_wrapper).sortable({
            connectWith: $('.'+opportunities_html_wrapper),
            delay: 150,
            stop: function( event, ui ) {
                //make updated opportunity consistent
                //RETRIEVE THE ITEM MOVED
                let item_moved = ui.item;
                //update values on db
                updateOpportunity(item_moved);
                //update values locally
                updateValuesLocally();

            }
        });

    });

};
window.updateOpportunity = function(item_moved) {
    //GET INFORMATION OF OPPORTUNITY
    let opportunity = item_moved.attr("data-id");
    let order = item_moved.index()+1;
    //RETRIEVE THE STAGE FROM PARENT
    let opportunity_stage_item = item_moved.closest(".opportunities-stage-wrapp");
    //RETRIEVE THE STAGE
    let stage = opportunity_stage_item.attr("data-id");
    console.log("Opportunity: "+opportunity);
    console.log("Order: "+ order);
    console.log("Moved to stage: "+stage);

    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: opportunity_update_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            stage: stage,
            opportunity: opportunity,
            order: order,
        },
        success: function (data) {
            if (data == "200") {
                //try again
                console.log("Se actualizó la oportunidad con éxito.");
                return;
            }else{
                alert("Ocurrió un error, por favor cargue la página e intente de nuevo la operación.")
            }
            console.log(data);
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }else{
                alert("Ocurrió un error, por favor cargue la página e intente de nuevo la operación.")
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });


}
window.updateValuesLocally = function() {

    $( ".opportunities-stage-wrapp" ).each(function( index ) {
        let opportunities_child = $(this).find(".opportunity-wrapp");
        let opportunities_count = opportunities_child.length;
        let stage_id = $(this).attr("data-id");

        $(this).find(".opportunities-stage-count").html(opportunities_count);
        //opportunity-stage-total-amount
        let total_amount = 0;
        $(opportunities_child).each(function( index ) {
            let opportunity_price = parseFloat($(this).attr("data-price"));
            total_amount+=opportunity_price;
            //console.log( "Opportunity price: "+opportunity_price);
        });
        $(this).find(".opportunity-stage-total-amount-"+stage_id).html(total_amount);
        //console.log( "Opportunities on stage "+stage_id+": "+opportunities_count);
        //console.log( "Price on stage "+stage_id+": "+total_amount);
    });

}


window.addTotalPrice = function(stage_id, stage_price_route, opportunities_price_html_wrapper, search_input, userDashboard){
    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: stage_price_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            stage_id: stage_id,
            search_input: search_input,
            user_dashboard: userDashboard,
        },
        success: function (data) {
            //console.log("-----------------STAGE "+stage_id+"----------------");
            //console.log(data);
            $("."+opportunities_price_html_wrapper+"-"+stage_id).html(data);
            //console.log("-----------------STAGE "+stage_id+"----------------");

        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });
}



window.search = function(element, e) {
    console.log("evento: "+e.type);

    let model_html_wrapper = element.closest(".list-model-wrapp").attr("id");
    let search_input = element.closest(".list-model-wrapp").find(".search_input").val();
    let url = element.closest(".list-model-wrapp").find(".search_input").attr('data-href');

    $("#"+model_html_wrapper).append('Cargando los datos...');
    var url_without_parameters = url.split('?')[0];
    var page = getURLParameter(url, 'page');

    console.log("Model html wrapper: "+model_html_wrapper);
    console.log("url: "+url);
    console.log("search input: "+search_input);
    console.log("url without parameters: "+url_without_parameters);
    console.log("Page: "+page);

    addStageList(page, url_without_parameters, model_html_wrapper, search_input);
}


//DASHBOARD OPPORTUNITIES
window.addCompanyContacts =  function(company, company_contacts_route){
    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: company_contacts_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            company: company,
        },
        success: function (data) {
            console.log(data);
            $('#company_contact_id').html("");
            $(data).each(function (index,value) {
                $('#company_contact_id').append($('<option>', {
                    value: value.id,
                    text : value.name+" "+value.last_name
                }));
            });

        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });
};
//OPPORTUNITY DASHBOARD

//REPORTS



window.getAndDrawUsersOpportunities =  function(timePeriod, users_opportunities_sum_route, canvas_id, type){
    //Enviamos una solicitud con el ruc de la empresa
    $.ajax({
        type: 'GET', //THIS NEEDS TO BE GET
        url: users_opportunities_sum_route,
        tryCount : 0,
        retryLimit : 3,
        dataType : "json",
        data:{
            timePeriod: timePeriod,
        },
        success: function (data) {
            console.log("-----------------users Opportunities Sum----------------");
            console.log(data);
            //UPDATE VALUE GLOBALLY
            window.chartUserOpportunities = drawVerticalBar(data, canvas_id, chartUserOpportunities);
        },error:function(data){
            console.log("-----------------ERROR----------------");
            this.tryCount++;
            console.log("Try: "+this.tryCount+" out of "+this.retryLimit+".");
            console.log(data);
            if (this.tryCount <= this.retryLimit) {
                //try again
                $.ajax(this);
                return;
            }
            console.log(data);
            console.log("-----------------ERROR----------------");
        }
    });
};

window.drawChart =  function(data, canvas_id, chart, title, chart_type){
    let data_length = data.length;

    let randomColors = randomColorGraph(data_length);

    let labels_arr = [];
    let data_arr = [];
    $(data).each(function (index,value) {
        let name = value.name;
        let sum = value.sum;
        labels_arr.push(name);
        data_arr.push(sum);
    });

    //DIBUJAMOS LA DATA
    var ctx = $('#'+canvas_id);
    chart = new Chart(ctx, {
        type: chart_type,
        data: {
            labels: labels_arr,
            datasets: [{
                label: title,
                data: data_arr,
                backgroundColor: randomColors.graphColors,
                hoverBackgroundColor: randomColors.hoverColor,
                borderColor: randomColors.graphOutlines,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

};


window.randomColorGraph =  function(length){
    //let randomColors = [];
    var graphColors = [];
    var graphOutlines = [];
    var hoverColor = [];
    let i = 0;

    while (i <= length) {
        var randomR = Math.floor((Math.random() * 130) + 100);
        var randomG = Math.floor((Math.random() * 130) + 100);
        var randomB = Math.floor((Math.random() * 130) + 100);

        var graphBackground = "rgb("
            + randomR + ", "
            + randomG + ", "
            + randomB + ")";
        graphColors.push(graphBackground);

        var graphOutline = "rgb("
            + (randomR - 80) + ", "
            + (randomG - 80) + ", "
            + (randomB - 80) + ")";
        graphOutlines.push(graphOutline);

        var hoverColors = "rgb("
            + (randomR + 25) + ", "
            + (randomG + 25) + ", "
            + (randomB + 25) + ")";
        hoverColor.push(hoverColors);

        i++;
    }

    /*
    randomColors.push(graphColors);
    randomColors.push(hoverColor);
    randomColors.push(graphOutlines);
*/
    return randomColors = {
        "graphColors": graphColors,
        "hoverColor": hoverColor,
        "graphOutlines": graphOutlines,
    };

};

window.updateChartByType =  function(type){
    //let randomColors = [];
    var graphColors = [];
    var graphOutlines = [];
    var hoverColor = [];
    let i = 0;

    while (i <= length) {
        var randomR = Math.floor((Math.random() * 130) + 100);
        var randomG = Math.floor((Math.random() * 130) + 100);
        var randomB = Math.floor((Math.random() * 130) + 100);

        var graphBackground = "rgb("
            + randomR + ", "
            + randomG + ", "
            + randomB + ")";
        graphColors.push(graphBackground);

        var graphOutline = "rgb("
            + (randomR - 80) + ", "
            + (randomG - 80) + ", "
            + (randomB - 80) + ")";
        graphOutlines.push(graphOutline);

        var hoverColors = "rgb("
            + (randomR + 25) + ", "
            + (randomG + 25) + ", "
            + (randomB + 25) + ")";
        hoverColor.push(hoverColors);

        i++;
    }

    /*
    randomColors.push(graphColors);
    randomColors.push(hoverColor);
    randomColors.push(graphOutlines);
*/
    return randomColors = {
        "graphColors": graphColors,
        "hoverColor": hoverColor,
        "graphOutlines": graphOutlines,
    };

};
//REPORTS
//SELECT LIST
$(document).on('click', '.dropdown-services-list', function (e) {
    e.stopPropagation();
});
//SELECT LIST

window.getMonthName =  function(month){
    month_name = "";

    switch (parseInt(month)) {
        case 1:
            month_name = "enero";
            break;
        case 2:
            month_name = "febrero";
            break;
        case 3:
            month_name = "marzo";
            break;
        case 4:
            month_name = "abril";
            break;
        case 5:
            month_name = "mayo";
            break;
        case 6:
            month_name = "junio";
            break;
        case 7:
            month_name = "julio";
            break;
        case 8:
            month_name = "agosto";
            break;
        case 9:
            month_name = "septiembre";
            break;
        case 10:
            month_name = "octubre";
            break;
        case 11:
            month_name = "noviembre";
            break;
        case 12:
            month_name = "diciembre";
            break;

    }


    return month_name;

};
window.getMonthDescription =  function(months){
    month_description = "";

    let months_number = 0;
    $.each(months, function( index, value ) {
        months_number++;
        let month_name = getMonthName(value);
        if(months_number == 1){
            month_description+=month_name;
        }else{
            month_description+=(", "+month_name);
        }
    });

    if(!months_number){
        month_description="todos los meses";
    }
    return month_description;

};
